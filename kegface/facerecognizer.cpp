#include "facerecognizer.h"
#include <iostream>

FaceRecognizer::FaceRecognizer()
{
    std::cout << "Creating FaceRecognizer"<<std::endl;
    //haar cascades are used for facial detection (to determine where and how many faces there are)
    haar_path = "/usr/share/opencv/haarcascades/haarcascade_frontalface_default.xml";//todo - detect this somehow
    haar_cascade.load(haar_path);

    //face recognition model
    model = cv::createLBPHFaceRecognizer();
}
FaceRecognizer::~FaceRecognizer(){
    std::cout << "Deallocating FaceRecognizer"<< std::endl;
}

void FaceRecognizer::find_faces(cv::Mat& source, std::vector<cv::Rect_<int> >& faces){
    haar_cascade.detectMultiScale(source, faces, 1.1,3,0,cv::Size(80,80), cv::Size(300,300));
}

void FaceRecognizer::train_facedata(std::vector<std::string> imagedirs, std::vector<int> ids){
    this->images.clear();
    for(std::vector<std::string>::iterator it = imagedirs.begin() ; it != imagedirs.end(); ++it){
        std::cout<<"loading "<<*it<<std::endl;
        images.push_back(cv::imread(*it,0));
    }
    this->labels.clear();
    for(std::vector<int>::iterator it = ids.begin() ; it != ids.end(); ++it){
        labels.push_back(*it);
    }


    //model->train(images,labels);//this may take a while depending on how many faces you have
}

int FaceRecognizer::identify(cv::Mat &area, double& confidence){
    int label = -1;\
    //model->predict(area, label, confidence);
    return label;
}
