#include "kegdbus.h"
#include "kegdbusadaptor.h"

kegdbus::kegdbus(QObject *parent):
    QObject(parent)
{ //things?
    new kegdbusAdaptor(this);

    QDBusConnection dbus = QDBusConnection::sessionBus();

    dbus.registerObject("/com/timsiggins/kegobot", this);

    dbus.registerService("com.timsiggins.kegobot");
    std::cout<<"Dbus should be running..."<<std::endl;
}

void kegdbus::onStartPour(){
    std::cout<<"Pour start"<<std::endl;
}

void kegdbus::onEndPour(double pints){
    std::cout<<"Pour complete:"<<""<<" pints"<<std::endl;

}

void kegdbus::onStatusReport(QString status){

}


QString kegdbus::echo(QString blah){
    std::cout<<"echoing "<<blah.toStdString()<<std::endl;
    return blah;
}
