#ifndef KEGDBUS_H
#define KEGDBUS_H

#include <QObject>
#include <QtDBus/QDBusConnection>
#include <iostream>

class kegdbus: public QObject
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", "com.timsiggins.kegobot")

public:
    explicit kegdbus(QObject* parent = 0);

Q_SIGNALS:
    Q_SCRIPTABLE void statusReport(QString status);
    void onPourStarted();
    void onPourEnded();
    void onStatusReported();

public Q_SLOTS:
    void onStartPour();
    void onEndPour(double pints);
    void onStatusReport(QString status);
    QString echo(QString blah);


};

#endif // KEGDBUS_H
