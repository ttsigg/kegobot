#ifndef DATABASE_H
#define DATABASE_H
#define SERVER "tcp://localhost"
#define USER "root"
#define PASSWORD "test-password"
#define DATABASE "kegface"
#define PATH_PREFIX "/home/tim/IdeaProjects/kegweb/media/"

#include <mysql_connection.h> 

#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <cppconn/prepared_statement.h>
#include <sstream>
#include <string>
#include <vector>
namespace db{
    class Database;
}

class Database
{
public:
    Database();
    ~Database();
    void load_faces(std::vector<std::string>& images,std::vector<int>& ids);
    void test_connection();
    std::string getUser(int i);

private:
    sql::Driver *driver;
    sql::Connection *con;

};


    
    

#endif // DATABASE_H
