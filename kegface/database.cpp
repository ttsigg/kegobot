#include "database.h"

Database::Database()
{
    std::cout<<"initializing database"<<std::endl;
    try {

        /* Create a connection */
        driver = get_driver_instance();
        con = driver->connect(SERVER, USER, PASSWORD);
        /* Connect to the MySQL test database */
        con->setSchema(DATABASE);
    } catch (sql::SQLException &e) {
        std::cout << "# ERR: SQLException in " << __FILE__;
        std::cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << std::endl;
        std::cout << "# ERR: " << e.what();
        std::cout << " (MySQL error code: " << e.getErrorCode();
        std::cout << ", SQLState: " << e.getSQLState() << " )" << std::endl;
    }
}
Database::~Database(){
    //I don't know about c++, why does this deallocate my connection before I'm done with the class?
    //delete con;
    std::cout<<"Delorting database"<<std::endl;
}

void Database::load_faces(std::vector<std::string>& images, std::vector<int>& ids)
{

    sql::PreparedStatement *stmt;
    sql::ResultSet *res;
    try{
        stmt = con->prepareStatement("SELECT user_id,photo FROM facedata ORDER BY user_id");
        res = stmt->executeQuery();
        while (res->next()){
            std::stringstream path;
            path << PATH_PREFIX << res->getString("photo");
            images.push_back(path.str());
            ids.push_back(res->getInt("user_id"));
        }
    } catch (sql::SQLException &e) {
        std::cout << "# ERR: SQLException in " << __FILE__;
        std::cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << std::endl;
        std::cout << "# ERR: " << e.what();
        std::cout << " (MySQL error code: " << e.getErrorCode();
        std::cout << ", SQLState: " << e.getSQLState() << " )" << std::endl;
    }
}

void Database::test_connection(){
    sql::Statement *stmt;
    sql::ResultSet *res;
    try{
        stmt = con->createStatement();
        res = stmt->executeQuery("SELECT 'Hello World!' AS _message");
        while (res->next()) {
            std::cout << "\t... MySQL replies: ";
            /* Access column data by alias or column name */
            std::cout << res->getString("_message") << std::endl;
            std::cout << "\t... MySQL says it again: ";
            /* Access column fata by numeric offset, 1 is the first column */
            std::cout << res->getString(1) << std::endl;
        }
        delete res;
        delete stmt;
    } catch (sql::SQLException &e) {
        std::cout << "# ERR: SQLException in " << __FILE__;
        std::cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << std::endl;
        std::cout << "# ERR: " << e.what();
        std::cout << " (MySQL error code: " << e.getErrorCode();
        std::cout << ", SQLState: " << e.getSQLState() << " )" << std::endl;
    }
}

std::string Database::getUser(int i){
    sql::PreparedStatement *stmt;
    sql::ResultSet *res;
    std::string result = "Not Found";
    try{
        stmt = con->prepareStatement("SELECT username FROM auth_user WHERE id=? LIMIT 1");
        stmt->setInt(1,i);
        res = stmt->executeQuery();
        if (res->first()) {
            result = res->getString("username");
        }
        delete res;
        delete stmt;
    } catch (sql::SQLException &e) {
        std::cout << "# ERR: SQLException in " << __FILE__;
        std::cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << std::endl;
        std::cout << "# ERR: " << e.what();
        std::cout << " (MySQL error code: " << e.getErrorCode();
        std::cout << ", SQLState: " << e.getSQLState() << " )" << std::endl;
    }
    return result;
}


