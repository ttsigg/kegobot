#-------------------------------------------------
#
# Project created by QtCreator 2014-04-12T12:49:29
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = kegface
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    database.cpp \
    facerecognizer.cpp \
    kegdbus.cpp \
    kegdbusadaptor.cpp \
    asdf.cpp

HEADERS  += mainwindow.h \
    database.h \
    facerecognizer.h \
    kegdbus.h \
    kegdbusadaptor.h \
    asdf.h

FORMS    += mainwindow.ui

LIBS += -L/usr/local/lib
LIBS += -lopencv_core
LIBS += -lopencv_imgproc
LIBS += -lopencv_highgui
LIBS += -lopencv_ml
LIBS += -lopencv_video
LIBS += -lopencv_features2d
LIBS += -lopencv_calib3d
LIBS += -lopencv_objdetect
LIBS += -lopencv_contrib
LIBS += -lopencv_legacy
LIBS += -lopencv_flann
LIBS += -lmysqlcppconn

QT += dbus


OTHER_FILES += \
    MainUI.qml \
    dbus/com.timsiggins.kegobot.xml \
    keg.qml \
    video.qml
