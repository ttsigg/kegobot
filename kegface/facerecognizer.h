#ifndef FACERECOGNIZER_H
#define FACERECOGNIZER_H
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/contrib/contrib.hpp>
#include <opencv2/objdetect/objdetect.hpp>

class FaceRecognizer
{
public:
    FaceRecognizer();
    ~FaceRecognizer();
    void process_face();
    void find_faces(cv::Mat& source, std::vector<cv::Rect_<int> >& faces);
    void train_facedata(std::vector<std::string> imagedirs, std::vector<int> ids);
    int identify(cv::Mat& area, double& confidence);

private:
    //for classifying faces
    cv::CascadeClassifier haar_cascade;
    //for use when recognizing faces
    cv::Ptr<cv::FaceRecognizer> model;

    std::vector<cv::Mat> images;
    std::vector<int> labels;

    //the size of the images used to train the face recognizer
    int imageHeight;
    int imageWidth;

    std::string haar_path;

};

#endif // FACERECOGNIZER_H
