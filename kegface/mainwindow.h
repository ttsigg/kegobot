#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <facerecognizer.h>
#include <database.h>
#include <kegdbus.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/contrib/contrib.hpp>
#include <opencv2/objdetect/objdetect.hpp>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void read_csv(const std::string& filename, std::vector<cv::Mat>& images, std::vector<int>& labels, char separator = ';');
    static cv::Mat norm_0_255(cv::InputArray _src);
    static int count;


public slots:
    void processFrameAndUpdateGui();


private slots:
    void on_btnStartStop_clicked();

private:
    Ui::MainWindow *ui;

    cv::VideoCapture capwebcam;
    cv::Mat matRaw;
    cv::Mat matProcessed;


    QImage qiRaw;
    QImage qiProcessed;

    std::vector<cv::Vec3f> vecCircles;
    std::vector<cv::Vec3f>::iterator iterCircles;

    QTimer* timer;

    std::vector< cv::Rect_<int> > faces;
    short scan;
    std::string dbg1;

    FaceRecognizer recognizer;
    Database database;

    int userid = 0;

    kegdbus *drunkbus;


};

#endif // MAINWINDOW_H
