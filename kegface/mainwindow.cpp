#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QtCore>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>



MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    drunkbus(new kegdbus(parent))
{
    ui->setupUi(this);

    capwebcam.open(0);

    if(!capwebcam.isOpened()){
        ui->txtStatus->appendPlainText("ERROR: Could not open webcam!\n Are you sure you have one?");;
        return;
    }
    database.test_connection();

    std::vector<std::string> paths;
    std::vector<int> ids;
    database.load_faces(paths,ids);
    recognizer.train_facedata(paths,ids);

    scan = 0;
    dbg1 = "Initial value";

    timer = new QTimer(this);
    connect(timer,SIGNAL(timeout()),this,SLOT(processFrameAndUpdateGui()));
    timer->start(33);//17 = 60fps, 33=30fps
}

void MainWindow::processFrameAndUpdateGui(){
    capwebcam >> matRaw;//read frame into matRaw
    if(matRaw.empty()) {
        return;
    }
    cv::flip(matRaw,matRaw,1);//mirror image (because it looks wierd otherwise)

    cv::cvtColor(matRaw, matProcessed, CV_BGR2GRAY);
    // Find the faces in the frame:
    //    std::vector< cv::Rect_<int> > newFaces;

    std::stringstream dbg;
    if (scan % 5 == 0){
        recognizer.find_faces(matProcessed, faces);

        scan = 0;
        std::stringstream fmt;
        fmt << "Found "<<faces.size()<<" faces";
        dbg1 = fmt.str();
    }
    scan++;
    dbg<<dbg1<<std::endl<<"Scan "<<scan<<std::endl;


    for(int i = 0; i < faces.size(); i++) {
        // Process face by face:
        cv::Rect face_i = faces[i];
        // Crop the face from the image. So simple with OpenCV C++:
        cv::Mat face = matProcessed(face_i);

        // Resizing the face is necessary for Eigenfaces and Fisherfaces. You can easily
        // verify this, by reading through the face recognition tutorial coming with OpenCV.
        // Resizing IS NOT NEEDED for Local Binary Patterns Histograms, so preparing the
        // input data really depends on the algorithm used.
        //
        // I strongly encourage you to play around with the algorithms. See which work best
        // in your scenario, LBPH should always be a contender for robust face recognition.
        //
        // Since I am showing the Fisherfaces algorithm here, I also show how to resize the
        // face you have just found:
        cv::Mat face_resized;
        cv::resize(face, face_resized, cv::Size(250, 250), 1.0, 1.0, cv::INTER_CUBIC);
        // Now perform the prediction, see how easy that is:
        //int prediction = model->predict(face_resized);
        // And finally write all we've found out to the original image!
        // First of all draw a green rectangle around the detected face:
        cv::rectangle(matRaw, face_i, CV_RGB(0, 255,0), 1);
        // Create the text we will annotate the box with:
//        if (scan == 0){
        double confidence = 0.0;
            userid = recognizer.identify(face_resized, confidence);
//        }
        dbg<<"userid="<<userid<<std::endl;

        std::stringstream box_text;
        box_text << database.getUser(userid) << "(" << std::setprecision(4)<<confidence<<")";

        // Calculate the position for annotated text (make sure we don't
        // put illegal values in there):
        int pos_x = std::max(face_i.tl().x - 10, 0);
        int pos_y = std::max(face_i.tl().y - 10, 0);
        // And now put it into the image:
        cv::putText(matRaw, box_text.str(), cv::Point(pos_x, pos_y), cv::FONT_HERSHEY_PLAIN, 1.0, CV_RGB(0,255,0), 2.0);
    }

    ui->txtStatus->setPlainText(QString::fromStdString(dbg.str()));


    //convert cv mat to qt qimage or whatever
    cv::cvtColor(matRaw,matRaw,CV_BGR2RGB);
    QImage qimgRaw((uchar*)matRaw.data,matRaw.cols,matRaw.rows,matRaw.step,QImage::Format_RGB888);
    //    QImage qimgProcessed((uchar*)matProcessed.data,matProcessed.cols,matProcessed.rows,matProcessed.step,QImage::Format_RGB888);

    ui->lblRaw->setPixmap(QPixmap::fromImage(qimgRaw));
    //ui->lblProcessed->setPixmap(QPixmap::fromImage(qimgProcessed));



}


MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_btnStartStop_clicked()
{
    if (timer->isActive())   {
        timer->stop();
    } else {
        timer->start();
    }
}


void MainWindow::read_csv(const std::string& filename, std::vector<cv::Mat>& images, std::vector<int>& labels, char separator) {
    std::ifstream file(filename.c_str(), std::ifstream::in);
    if (!file) {
        std::string error_message = "No valid input file was given, please check the given filename.";
        CV_Error(CV_StsBadArg, error_message);
    }
    std::string line, path, classlabel;
    while (getline(file, line)) {
        std::stringstream liness(line);
        getline(liness, path, separator);
        getline(liness, classlabel);
        if(!path.empty() && !classlabel.empty()) {
            images.push_back(cv::imread(path, 0));
            labels.push_back(atoi(classlabel.c_str()));
        }
    }
}

static cv::Mat norm_0_255(cv::InputArray _src) {
    cv::Mat src = _src.getMat();
    // Create and return normalized image:
    cv::Mat dst;
    switch(src.channels()) {
    case 1:
        cv::normalize(_src, dst, 0, 255, cv::NORM_MINMAX, CV_8UC1);
        break;
    case 3:
        cv::normalize(_src, dst, 0, 255, cv::NORM_MINMAX, CV_8UC3);
        break;
    default:
        src.copyTo(dst);
        break;
    }
    return dst;
}
