kegobot
=======
The kegobot 3000 system is a comprehensive keg management system.  This system includes support for flow control (coming soon), flow monitoring, facial recognition, user consumption tracking, NFC tags (coming soon), and social media posting.

**kegd**    
This is the daemon which should run on the raspberry pi.  This must run as root and will handle all interaction with the sensors

**kegface**    
The UI component, this is the main part shown to the users.  Faces will be captured and recognized on this component.

**kegweb**    
The web management interface.  A django site which will allow you to monitor temperatures, usage, etc.  This is where you load faces into the facial recognition software.

