
#!/usr/bin/env python3.4  
# script by Alex Eames http://RasPi.tv  
# http://RasPi.tv/how-to-use-interrupts-with-python-on-the-raspberry-pi-and-rpi-gpio-part-3  
import RPi.GPIO as GPIO  
import time
from subprocess import Popen

class Counter:
    calibration = 210.3 #pulses per pint
    camera = Popen('ls')
    pours=0

    def __init__(self,pin_num):
        GPIO.setup(pin_num,GPIO.IN,pull_up_down=GPIO.PUD_UP)
        self.pin = pin_num
        self.count = 0
        self.last = 0
        self.pulsing = False
        self.start = 0
    
    #called when a pulse is detected
    def callback(self,pin):
        #print("pulse " + str(self.count))
        self.count += 1
        if (!self.pulsing and Counter.camera.poll() == None):
            Counter.camera = Popen(["/opt/vc/bin/raspistill","-o",str(self.pours)+"-pin"+str(self.pin)+".jpg"]
            self.pours+=1
        self.pulsing = True

    
    #start the counter
    def go(self):
        GPIO.add_event_detect(self.pin, GPIO.FALLING,
                callback=self.callback)
    
    #returns true if there hasn't been a change in the count since last check 
    def count_ready(self):
        if (self.pulsing and self.count == self.last):
            return True
        elif (self.pulsing):
            total = self.count - self.last
            self.last = self.count
            print(str(total)+" pulses since last check")
        
        return False
    
    #return the number of pulses on the counter
    def get_pulses(self):
        return (self.count - self.start)

    #get the number of pints on the counter
    def get_pints(self):
        return (self.count - self.start) / self.calibration

    #resets the count
    def reset(self):
        self.pulsing = False
        self.start = self.count


GPIO.setmode(GPIO.BCM)  
  
try:  
    pours=0
    c17 = Counter(17)
    c17.go()
    c18 = Counter(18)
    c18.go()
    while(True):
        time.sleep(3)
        if (c17.count_ready()):
            pours+=1
            pints = str(c17.get_pints())
            print("Pin 17 counted "+str(c17.get_pulses())+" pulses ("+pints+" pints)")
            c17.reset()
            #camera shit
            #call(["/opt/vc/bin/raspistill","-o",str(pours)+"-pin17-"+pints+".jpg"])

        if (c18.count_ready()):
            pours+=1
            pints = str(c18.get_pints())
            print("Pin 18 counted "+str(c18.get_pulses())+" pulses ("+pints+" pints)")
            c18.reset()
            #camera shit
            #call(["/opt/vc/bin/raspistill","-o",str(pours)+"-pin18-"+pints+".jpg"])

except KeyboardInterrupt:  
    GPIO.cleanup()       # clean up GPIO on CTRL+C exit  
    GPIO.cleanup()           # clean up GPIO on normal exit  

