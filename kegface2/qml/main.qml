import QtQuick 2.2
import QtQuick.Window 2.1
import "."


Rectangle {
    id: screen
    width: 1440; height: 900

    SystemPalette { id: activePalette }

    Item {//background
        width: parent.width
        anchors { top: parent.top; bottom: parent.bottom }

        Image {
            id: background
            anchors.fill: parent
            source: "images/bubbles1440.jpg"
            fillMode: Image.PreserveAspectFit
        }
    }
//top bar
    Rectangle {
        id: systemInfo
        width: parent.width/3; height: 120
        anchors.top: screen.top
        anchors.left: screen.left
        color:"transparent"

        Text {
            id: uptime
            anchors { left: parent.left; top: parent.top }
            text: "Uptime: 0d0h0m?"
            font.pointSize: Style.header.fontSize
            color: Style.textColor
        }
        Text {
            id: coretemp
            anchors { left: parent.left; verticalCenter: parent.verticalCenter }
            text: "Core Temp: 33C"
            font.pointSize: Style.header.fontSize
            color: Style.textColor

        }
        Text {
            id: load
            anchors { left: parent.left; bottom: parent.bottom }
            text: "CPU:33% MEM:54% SWP:00%"
            font.pointSize: Style.header.fontSize
            color: Style.textColor
        }
    }
    Rectangle {
        id: datetime
        width: parent.width/3
        height: 120
        anchors.left: systemInfo.right
        anchors.top: parent.top
        color:"transparent"
        Text{
            id:time
            height: 1
            font.pointSize: Style.header.titleSize
            color: Style.textColor
            text: "17:00"
            anchors.horizontalCenter: parent.horizontalCenter
        }
        Text{
            id:date
            text: "Sat May 24, 2014"
            anchors.bottom: parent.bottom
           font.pointSize: Style.header.subtitleSize
           color: Style.textColor
            anchors.horizontalCenter: parent.horizontalCenter
        }
    }
    Rectangle {
        id: infoPane2
        width: parent.width/3; height: 120
        anchors.top: screen.top
        anchors.left: datetime.right
        color:"transparent"
        Text {
            id: stuff
            anchors { left: parent.left; top: parent.top }
            text: "Not sure what to put here"
            color: Style.textColor
            font.pointSize: Style.header.fontSize
        }
    }
//bottom area
    Rectangle {
        id: footer
        height: 150
        width: parent.width
        anchors.bottom: parent.bottom
        color:"transparent"

    }


    //keg panes
    Rectangle {
        id: kegLeft
        width:400
        anchors.top:systemInfo.bottom
        anchors.left:parent.left
        anchors.bottom: footer.top
        anchors.margins:10
        color:"transparent"

        Image {
            id: iconLeft
            width: 100
            height: 100
            anchors.left: parent.left
            anchors.leftMargin: 0
            anchors.top: titleLeft.bottom
            anchors.topMargin: 0
            source: "images/beerred.png"
        }
        Text {
            id: titleLeft
            text: qsTr("Ozark Irish Red")
            horizontalAlignment: Text.AlignHCenter
            anchors.left: parent.left
            anchors.leftMargin: 10
            anchors.right: parent.right
            anchors.rightMargin: 10
            font.pointSize: Style.brew.titleSize
            font.bold: true
            color:Style.textColor
        }
        Text {
            id: styleLeft
            text: qsTr("Irish Red")
            anchors.top: titleLeft.bottom
            anchors.topMargin: 15
            anchors.left: iconLeft.right
            anchors.leftMargin: 10
            anchors.right: parent.right
            anchors.rightMargin: 10
            horizontalAlignment: Text.AlignHCenter
            font.pointSize: Style.brew.subtitleSize
            color: Style.textColor
                    }
        Rectangle {
            id: remainLeft
            color: "#ffffff"
            anchors.right: iconLeft.right
            anchors.rightMargin: 20
            anchors.left: parent.left
            anchors.leftMargin: 20
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 20
            anchors.top: iconLeft.bottom
            anchors.topMargin: 20
        }
        Rectangle{
            id: statsLeft
            anchors.top: styleLeft.bottom
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.left: iconLeft.right
            color:"transparent"

            Text {
                id: lblAbvLeft
                text: qsTr("ABV: ")
                font.pointSize: Style.brew.fontSize
                anchors.top: parent.top
                anchors.topMargin: 20
                anchors.right: parent.horizontalCenter
                color: Style.textColor2
            }
            Text{
                id:abvLeft
                text:qsTr(" 5.4%")
                font.pointSize: Style.brew.fontSize
                anchors.top: parent.top
                anchors.topMargin: 20
                anchors.left: parent.horizontalCenter
                color: Style.textColor2
            }

            Text {
                id: lblIbuLeft
                text: qsTr("IBU: ")
                font.pointSize: Style.brew.fontSize
                anchors.top: abvLeft.bottom
                anchors.topMargin: 20
                anchors.right: parent.horizontalCenter
                color: Style.textColor2
            }
            Text{
                id:ibuLeft
                text:qsTr("22")
                font.pointSize: Style.brew.fontSize
                anchors.top: abvLeft.bottom
                anchors.topMargin: 20
                anchors.left: parent.horizontalCenter
                color: Style.textColor2
            }
            Text {
                id: lblBrewedLeft
                text: qsTr("Brewed: ")
                font.pointSize: Style.brew.fontSize
                anchors.top: ibuLeft.bottom
                anchors.topMargin: 20
                anchors.right: parent.horizontalCenter
                color: Style.textColor2
            }
            Text{
                id:brewedLeft
                text:qsTr("3/14/14")
                font.pointSize: Style.brew.fontSize
                anchors.top: ibuLeft.bottom
                anchors.topMargin: 20
                anchors.left: parent.horizontalCenter
                color: Style.textColor2
            }
            Text {
                id: lblKeggedLeft
                text: qsTr("Kegged: ")
                font.pointSize: Style.brew.fontSize
                anchors.top: brewedLeft.bottom
                anchors.topMargin: 20
                anchors.right: parent.horizontalCenter
                color: Style.textColor2
            }
            Text{
                id:keggedLeft
                text:qsTr("4/14/14")
                font.pointSize: Style.brew.fontSize
                anchors.top: brewedLeft.bottom
                anchors.topMargin: 20
                anchors.left: parent.horizontalCenter
                color: Style.textColor2
            }
            Text {
                id: lblPoursLeft
                text: qsTr("Pours Today: ")
                font.pointSize: Style.brew.fontSize
                anchors.top: keggedLeft.bottom
                anchors.topMargin: 20
                anchors.right: parent.horizontalCenter
                color: Style.textColor2
            }
            Text{
                id:poursLeft
                text:qsTr("4")
                font.pointSize: Style.brew.fontSize
                anchors.top: keggedLeft.bottom
                anchors.topMargin: 20
                anchors.left: parent.horizontalCenter
                color: Style.textColor2
            }
            Text {
                id: lblTempLeft
                text: qsTr("Temperature: ")
                font.pointSize: Style.brew.fontSize
                anchors.top: poursLeft.bottom
                anchors.topMargin: 20
                anchors.right: parent.horizontalCenter
                color: Style.textColor2
            }
            Text{
                id:tempLeft
                text:qsTr("5.6°C")
                font.pointSize: Style.brew.fontSize
                anchors.top: poursLeft.bottom
                anchors.topMargin: 20
                anchors.left: parent.horizontalCenter
                color: Style.textColor2
            }
        }
    }


    Rectangle {
        id: kegRight
        width:400
        anchors.top:systemInfo.bottom
        anchors.right: parent.right
        anchors.bottom: footer.top
        anchors.topMargin:10
        anchors.bottomMargin:10
        anchors.leftMargin:5
        anchors.rightMargin:10
        border.color: "#028000"
        color:"transparent"


        Text {
            id: titleRight
            text: qsTr("Stalin's Corpse")
            horizontalAlignment: Text.AlignHCenter
            anchors.left: parent.left
            anchors.leftMargin: 10
            font.bold: true
            anchors.right: parent.right
            anchors.rightMargin: 10
            font.pointSize: Style.brew.titleSize
            color:Style.textColor
        }
        Image {
            id: iconRight
            width: 100
            height: 100
            anchors.right: parent.right
            anchors.leftMargin: 0
            anchors.top: titleRight.bottom
            anchors.topMargin: 0
            source: "images/beerdark.png"
        }
        Text {
            id: styleRight
            text: qsTr("Russian Imperial Stout")
            anchors.top: titleRight.bottom
            anchors.topMargin: 15
            anchors.left: parent.left
            font.pointSize: Style.brew.subtitleSize
            color: Style.textColor
        }
        Rectangle {
            id: remainRight
            color: "#ffffff"
            anchors.left: iconRight.left
            anchors.leftMargin: 20
            anchors.right: parent.right
            anchors.rightMargin: 20
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 20
            anchors.top: iconRight.bottom
            anchors.topMargin: 20
        }
        Rectangle{
            id: statsRight
            anchors.top: styleRight.bottom
            anchors.right: iconRight.left
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            color:"transparent"
            Text {
                id: lblAbvRight
                text: qsTr("ABV: ")
                font.pointSize: Style.brew.fontSize
                anchors.top: parent.top
                anchors.topMargin: 20
                anchors.right: parent.horizontalCenter
                color: Style.textColor2
            }
            Text{
                id:abvRight
                text:qsTr(" 5.4%")
                font.pointSize: Style.brew.fontSize
                anchors.top: parent.top
                anchors.topMargin: 20
                anchors.left: parent.horizontalCenter
                color: Style.textColor2
            }

            Text {
                id: lblIbuRight
                color: "#d4d4d4"
                text: qsTr("IBU: ")
                font.pointSize: Style.brew.fontSize
                anchors.top: abvRight.bottom
                anchors.topMargin: 20
                anchors.right: parent.horizontalCenter
            }
            Text{
                id:ibuRight
                text:qsTr("22")
                font.pointSize: Style.brew.fontSize
                anchors.top: abvRight.bottom
                anchors.topMargin: 20
                anchors.left: parent.horizontalCenter
                color: Style.textColor2
            }
            Text {
                id: lblBrewedRight
                text: qsTr("Brewed: ")
                font.pointSize: Style.brew.fontSize
                anchors.top: ibuRight.bottom
                anchors.topMargin: 20
                anchors.right: parent.horizontalCenter
                color: Style.textColor2
            }
            Text{
                id:brewedRight
                text:qsTr("3/14/14")
                font.pointSize: Style.brew.fontSize
                anchors.top: ibuRight.bottom
                anchors.topMargin: 20
                anchors.left: parent.horizontalCenter
                color: Style.textColor2
            }
            Text {
                id: lblKeggedRight
                text: qsTr("Kegged: ")
                font.pointSize: Style.brew.fontSize
                anchors.top: brewedRight.bottom
                anchors.topMargin: 20
                anchors.right: parent.horizontalCenter
                color: Style.textColor2
            }
            Text{
                id:keggedRight
                text:qsTr("4/14/14")
                font.pointSize: Style.brew.fontSize
                anchors.top: brewedRight.bottom
                anchors.topMargin: 20
                anchors.left: parent.horizontalCenter
                color: Style.textColor2
            }
            Text {
                id: lblPoursRight
                text: qsTr("Pours Today: ")
                font.pointSize: Style.brew.fontSize
                anchors.top: keggedRight.bottom
                anchors.topMargin: 20
                anchors.right: parent.horizontalCenter
                color: Style.textColor2
            }
            Text{
                id:poursRight
                text:qsTr("4")
                font.pointSize: Style.brew.fontSize
                anchors.top: keggedRight.bottom
                anchors.topMargin: 20
                anchors.left: parent.horizontalCenter
                color: Style.textColor2
            }
            Text {
                id: lblTempRight
                text: qsTr("Temperature: ")
                font.pointSize: Style.brew.fontSize
                anchors.top: poursRight.bottom
                anchors.topMargin: 20
                anchors.right: parent.horizontalCenter
                color: Style.textColor2
            }
            Text {
                id:tempRight
                color: "#d4d4d4"
                text:qsTr("5.6°C")
                font.pointSize: Style.brew.fontSize
                anchors.top: poursRight.bottom
                anchors.topMargin: 20
                anchors.left: parent.horizontalCenter
            }
        }
    }

//video
    Rectangle {
        id: video
        anchors.top: datetime.bottom
        anchors.margins: 5
        anchors.bottom: footer.top
        anchors.left: kegLeft.right
        anchors.right: kegRight.left
        color:"black"
        anchors.leftMargin: 0
        anchors.rightMargin: 0

    }


}
