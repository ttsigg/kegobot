pragma Singleton
import QtQuick 2.0

QtObject {
    property color textColor: "white"
    property color textColor2: "lightgray"

    property QtObject header: QtObject{
        property int titleSize: 38;
        property int subtitleSize: 24;
        property int fontSize: 18;
        property color headerColor: "#333333";
    }

    property QtObject brew: QtObject{
        property color background: "#00000088";
        property int titleSize: 24;
        property int subtitleSize: 18;
        property int fontSize: 12;
    }
}
