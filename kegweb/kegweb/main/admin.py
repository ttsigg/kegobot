from django.contrib import admin

# Register your models here.
from kegweb.main.models import Pour, Facedata, Event

admin.site.register(Pour)
admin.site.register(Facedata)
admin.site.register(Event)
