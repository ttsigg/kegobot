__author__ = 'tim'

from django.conf.urls import patterns, url

urlpatterns = patterns('kegweb.main.views',
                       url(r'^face/(?P<userid>\d+)/delete/(?P<faceid>\d+)/$', 'deleteface', name='deleteface'),
                       url(r'^face/(?P<userid>\d+)/$', 'face', name='face'),
                       )