from __future__ import unicode_literals
from django.contrib.auth.models import User
from kegweb import settings

__author__ = 'tim'

# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
# * Rearrange models' order
# * Make sure each model has one field with primary_key=True
# * Remove `managed = False` lines if you wish to allow Django to create and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin.py sqlcustom [appname]'
# into your database.

from django.db import models


class Event(models.Model):
    name = models.CharField(max_length=45)
    date = models.DateField()
    description = models.TextField()

    class Meta:
        db_table = 'event'


class Facedata(models.Model):
    user = models.ForeignKey(User)
    photo = models.ImageField(upload_to="facedb/", null=False, blank=False)
    added = models.DateTimeField(auto_now_add=True, blank=False, null=False)

    class Meta:
        db_table = 'facedata'


class KegUser(models.Model):
    user = models.OneToOneField(User)
    photo = models.ImageField(upload_to="profile/", null=True, blank=True)

    class Meta:
        db_table = 'keguser'


class Pour(models.Model):
    eventid = models.ForeignKey(Event, db_column='eventid', blank=True, null=True)
    user = models.ForeignKey(User, blank=True, null=True)
    volume = models.FloatField()
    pourphoto = models.ImageField(upload_to="pourimg/", blank=False, null=False)
    facebooked = models.BooleanField()
    time = models.DateTimeField()

    class Meta:
        db_table = 'pour'


class Userevent(models.Model):
    user = models.ForeignKey(User, blank=True, null=True)
    eventid = models.ForeignKey(Event, db_column='eventid', blank=True, null=True)
    firstpour = models.DateTimeField(blank=True, null=True)
    lastpour = models.DateTimeField(blank=True, null=True)

    class Meta:
        db_table = 'userevent'
