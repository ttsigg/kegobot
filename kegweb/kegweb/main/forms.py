__author__ = 'tim'

from django import forms


class FacedataForm(forms.Form):
    face = forms.FileField(

    )
    leftEyeX = forms.IntegerField(min_value=0, widget=forms.TextInput(attrs={'class':'xyinput left'}))
    rightEyeX = forms.IntegerField(min_value=0, widget=forms.TextInput(attrs={'class':'xyinput right'}))
    leftEyeY = forms.IntegerField(min_value=0, widget=forms.TextInput(attrs={'class':'xyinput left'}))
    rightEyeY = forms.IntegerField(min_value=0, widget=forms.TextInput(attrs={'class':'xyinput right'}))
