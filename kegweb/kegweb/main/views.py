# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse

from kegweb.main.models import Facedata
from kegweb.main.forms import FacedataForm
from facelib import crop_face


def face(request, userid=1):
    # Handle file upload
    if request.method == 'POST':
        form = FacedataForm(request.POST, request.FILES)
        if form.is_valid():
            # extract form data
            lx = int(request.POST['leftEyeX'])
            ly = int(request.POST['leftEyeY'])
            rx = int(request.POST['rightEyeX'])
            ry = int(request.POST['rightEyeY'])
            # crop the face
            face = crop_face.crop_form_image(request.FILES['face'], eye_left=(lx, ly), eye_right=(rx, ry))


            # send pic for processing

            # save facedata
            newdoc = Facedata(photo=face, user_id=userid)
            newdoc.save()

            # Redirect to the document list after POST
            return HttpResponseRedirect(reverse('kegweb.main.views.face', kwargs={'userid': userid}))
    else:
        form = FacedataForm()  # A empty, unbound form

    # Load documents for the list page
    documents = Facedata.objects.all().filter(user_id=userid)

    # Render list page with the documents and the form
    return render_to_response(
        'kegweb/faces.html',
        {'documents': documents, 'form': form, 'userid': userid},
        context_instance=RequestContext(request)
    )


def deleteface(request, faceid=0, userid=0):
    Facedata.objects.get(id=faceid).delete()
    return HttpResponse("yep")


def index():
    User.objects.get()
    return "derp"
