from django.conf.urls import patterns, include, url
from django.views.generic.base import RedirectView
from django.conf.urls.static import static
from django.conf import settings

from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
                       (r'^admin/',include(admin.site.urls)),
                       (r'^kegweb/', include('kegweb.main.urls')),
                       (r'^$', RedirectView.as_view(url='/kegweb/')),  # Just for ease of use.
                       ) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
